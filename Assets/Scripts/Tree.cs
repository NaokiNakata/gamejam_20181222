﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour
{
  // カップルのデータ
  private bool _hasMale = false;
  private bool _hasFemale = false;

  private bool _hasCouple = false;

  private int _maleID = -1;
  private int _femaleID = -1;

  private GameObject _male = null;

  private GameObject _female = null;

  // パラメータ
  [SerializeField]
  private int _hp = 100;

  [SerializeField]
  private int power = 100;

  [SerializeField]
  private int deleteTime = 5;

  [SerializeField]
  private int baseScore = 100;

  // パーティクルのオブジェクト
  [SerializeField]
  private GameObject SparkEffectObject;

  [SerializeField]
  private GameObject ExplosionEffectObject;

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    UpdateStatus();
  }

  void UpdateStatus()
  {
    if (_hasMale && _hasFemale && !_hasCouple)
    {
      _hasCouple = true;

      // 光るエフェクトの生成
      var instantiateEffect = GameObject.Instantiate(SparkEffectObject, transform.position + new Vector3(0f, 2f, 0f), Quaternion.identity) as GameObject;
      instantiateEffect.gameObject.transform.parent = this.gameObject.transform;
      Destroy(instantiateEffect, deleteTime);

      // スコア加算を開始
      StartCoroutine(AddScore());
    }
  }

  void OnCollisionEnter(Collision other)
  {
    string tag = other.gameObject.tag;
    int id = other.gameObject.GetInstanceID();
    if (tag == "Male")
    {
      if (!_hasMale)
      {
        _hasMale = true;
        _maleID = other.gameObject.GetInstanceID();
        _male = other.gameObject;
        other.gameObject.GetComponent<Human>().StopMove();
      }
      // 他の男がきたら吹き飛ばす
      else if (id != _maleID)
      {
        foreach (ContactPoint point in other.contacts)
        {
          Vector3 dir = (point.point - this.transform.position).normalized;

          //衝突位置
          other.gameObject.GetComponent<Rigidbody>().AddForce(dir * power, ForceMode.Impulse);
        }
      }
    }
    else if (tag == "Female")
    {
      if (!_hasFemale)
      {
        _hasFemale = true;
        _femaleID = other.gameObject.GetInstanceID();
        _female = other.gameObject;
        other.gameObject.GetComponent<Human>().StopMove();
      }
      // 他の女が来たら吹き飛ばす
      else if (id != _femaleID)
      {
        foreach (ContactPoint point in other.contacts)
        {
          Vector3 dir = (point.point - this.transform.position).normalized;

          //衝突位置
          other.gameObject.GetComponent<Rigidbody>().AddForce(dir * power, ForceMode.Impulse);
        }
      }
    }
    else if (tag == "Enemy")
    {
      _hp -= 15;
    }
  }


  IEnumerator AddScore()
  {
    if (_hp > 0)
    {
      // TODO: スコア加算
      ScoreManager.Instance.AddScore(baseScore);

      _hp -= 10;
      yield return new WaitForSeconds(0.5f);
      StartCoroutine(AddScore());
    }
    else
    {
      Explode();
      yield return null;
    }
  }

  // 爆発時の処理
  void Explode()
  {
    // 爆発エフェクトの生成
    var instantiateEffect = GameObject.Instantiate(ExplosionEffectObject, transform.position + new Vector3(0f, 2f, 0f), Quaternion.identity) as GameObject;
    Destroy(instantiateEffect, 1f);

    // 爆発時にボーナス得点
    ScoreManager.Instance.AddScore(baseScore * deleteTime);

    // オブジェクトの削除
    Destroy(_male);
    Destroy(_female);
    SetDisable();
  }

  // クリックで削除された時の処理
  public void Remove()
  {
    if (_male != null)
    {
      _male.GetComponent<Human>().StartMove();
    }

    if (_female != null)
    {
      _female.GetComponent<Human>().StartMove();
    }

    // 木の削除時に得点
    ScoreManager.Instance.AddScore((int)(baseScore * 0.2f));

    SetDisable();
  }

  // 自身を無効化(TreeManagerで削除)
  void SetDisable()
  {
    this.gameObject.SetActive(false);
  }

  public bool HasCouple()
  {
    return _hasCouple;
  }
}
