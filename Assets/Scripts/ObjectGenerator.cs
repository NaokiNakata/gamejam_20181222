﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGenerator : MonoBehaviour
{
  [SerializeField]
  GameObject[] prefabs;

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
    RaycastHit hit = new RaycastHit();

    if (Input.GetMouseButtonDown(0))
    {
      //レイを投げて何かのオブジェクトに当たった場合
      if (Physics.Raycast(ray, out hit))
      {
        //レイが当たった位置(hit.point)にオブジェクトを生成する
        ClickGenerate(prefabs[0], hit.point);
      }

    }
  }


  // 
  void ClickGenerate(GameObject prefab, Vector3 clickPosition)
  {
    // TODO: クリックされた座標のvalidation
    GameObject.Instantiate(prefab, clickPosition, Quaternion.identity);
  }
}
