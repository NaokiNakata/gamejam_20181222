﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManager : SingletonMonoBehaviour<CharacterManager>
{
  [SerializeField]
  float height;
  [SerializeField]
  float width;

  [SerializeField]
  float hOffset;
  [SerializeField]
  float wOffset;

  [SerializeField]
  GameObject MalePrefab;

  [SerializeField]
  GameObject FemalePrefab;

  [SerializeField]
  GameObject EnemyPrefab;

  [SerializeField]
  float waitTimeMin = 1f;

  private bool _isGenerating = false;

  // ゲーム開始からの経過秒数
  private float _timer;

  // Use this for initialization
  void Start()
  {
  }

  // Update is called once per frame
  void Update()
  {
    // Target生成状態の処理
    if (_isGenerating)
    {
      // timerを更新
      if (_timer > 0f)
      {
        _timer -= Time.deltaTime;
      }
    }
  }

  // ゲームを開始
  public void StartGame(float gameTime)
  {
    Debug.Log("Start: " + gameTime + "sec");

    _isGenerating = true;

    // 経過時間を初期化
    _timer = gameTime;

    // それぞれの生成を開始
    StartCoroutine(StartGeneratingHuman());
    StartCoroutine(StartGeneratingEnemy());
  }

  public void StopGame()
  {
    Debug.Log("Stop");
    _isGenerating = false;
  }

  // 一般人の生成
  public IEnumerator StartGeneratingHuman()
  {
    while (_isGenerating)
    {
      // ランダムな秒数待機してから生成
      float waitTimeMax = waitTimeMin + _timer / 15f; // 0 ~ 2.0
      yield return new WaitForSeconds(Random.Range(waitTimeMin, waitTimeMax));

      // ランダムな位置に生成
      float genPosX = Random.Range(-width / 2f, width / 2f);
      float genPosZ = Random.Range(-height / 2f, height / 2f);
      GameObject.Instantiate(MalePrefab, new Vector3(genPosX, 0.5f, genPosZ), Quaternion.identity);

      genPosX = Random.Range(-width / 2f, width / 2f);
      genPosZ = Random.Range(-height / 2f, -height / 2f);
      GameObject.Instantiate(FemalePrefab, new Vector3(genPosX, 0.5f, genPosZ), Quaternion.identity);
    }
  }

  // 敵の生成
  public IEnumerator StartGeneratingEnemy()
  {
    while (_isGenerating)
    {
      // ランダムな秒数待機してから生成
      float waitTimeMax = waitTimeMin * 4 + _timer / 15f; // 0 ~ 2.0
      yield return new WaitForSeconds(Random.Range(waitTimeMin, waitTimeMax));

      // ランダムな位置に生成
      float genPosX = Random.Range(-width / 2f, width / 2f);
      float genPosZ = Random.Range(-height / 2f, height / 2f);
      GameObject.Instantiate(EnemyPrefab, new Vector3(genPosX, 0.5f, genPosZ), Quaternion.identity);
    }
  }
}
