﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

  }

  void OnCollisionEnter(Collision other)
  {
    if (other.gameObject.tag == "Enemy")
    {
      // 自身を無効化(TrapManagerで削除)
      this.gameObject.SetActive(false);
    }
  }
}
