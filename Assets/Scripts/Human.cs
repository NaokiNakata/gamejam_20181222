﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Human : MonoBehaviour
{
  [SerializeField]
  float speed = 1; // 移動速度

  // 目標物を見つけているか
  private bool _isFound = false;

  // 移動状態か(目的地についている時はfalse)
  private bool _isMovable = true;

  // 目標の方向
  private Vector3 _distDirection;

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    // 移動
    if (_isMovable)
    {
      // 目標地点の更新
      UpdateTarget();
      this.transform.position += _distDirection * speed / 100f;
    }
  }

  // 目標地点を更新する
  protected void UpdateTarget()
  {
    Vector3 nearPos = TreeManager.Instance.GetNearestTreePosition(this.transform.position, false);

    // 最寄りの木の座標を更新
    if (nearPos != new Vector3(-1, -1, -1))
    {
      // 目標の方向へ回転
      RotateToTargetDirection(nearPos);

      // 目標の方向を更新
      _distDirection = (nearPos - this.transform.position).normalized;
    }
    else
    {
      // 木がない場合は停止
      _distDirection = new Vector3(0, 0, 0);
    }
  }

  // 目標の方向へ回転する
  void RotateToTargetDirection(Vector3 targetPosition)
  {
    Quaternion rotation = Quaternion.LookRotation(targetPosition - this.transform.position);
    rotation.x = rotation.z = 0;

    this.transform.rotation = rotation;
  }

  // 移動状態にする
  public void StartMove()
  {
    _isMovable = true;
  }

  // 停止する
  public void StopMove()
  {
    _isMovable = false;
  }
}
