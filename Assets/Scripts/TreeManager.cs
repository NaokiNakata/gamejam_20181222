﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeManager : SingletonMonoBehaviour<TreeManager>
{
  [SerializeField]
  GameObject TreePrefab;

  [SerializeField]
  int TreeCost = 500;

  private List<GameObject> _treesArray = new List<GameObject>();

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    // 左クリックで木を置く
    if (Input.GetMouseButtonDown(0))
    {
      Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
      RaycastHit hit = new RaycastHit();

      //レイを投げて何かのオブジェクトに当たった場合
      if (Physics.Raycast(ray, out hit))
      {
        if (hit.collider.tag == "Ground")
        {
          //レイが当たった位置(hit.point)にオブジェクトを生成する
          ClickGenerate(TreePrefab, hit.point);
        }
        else if (hit.collider.tag == "Tree")
        {
          hit.collider.gameObject.GetComponent<Tree>().Remove();
        }
      }
    }
  }

  // 木のリストに追加
  public void AddTree(GameObject tree)
  {
    _treesArray.Add(tree);
  }

  // クリックした位置に木を生成する
  void ClickGenerate(GameObject prefab, Vector3 clickPosition)
  {
    // TODO: クリックされた座標のvalidation
    GameObject tree = GameObject.Instantiate(prefab, clickPosition, Quaternion.identity);
    AddTree(tree);

    // 木の生成コスト分スコアを減点
    ScoreManager.Instance.AddScore(-TreeCost);
  }

  // 与えられたキャラクターの座標に最も近い木の座標を返す
  public Vector3 GetNearestTreePosition(Vector3 charaPosition, bool isFindingCouple)
  {
    Vector3 result = new Vector3(-1, -1, -1);
    float minDist = 0;

    bool isFound = false;
    foreach (GameObject tree in _treesArray)
    {
      // 無効状態なら削除
      if (!tree.gameObject.activeSelf)
      {
        _treesArray.Remove(tree);
        Destroy(tree.gameObject);
      }


      // 普通の木を探しているときのカップル or カップルを探している時の普通の木 はスキップ
      if (!isFindingCouple && tree.GetComponent<Tree>().HasCouple()
          || isFindingCouple && !tree.GetComponent<Tree>().HasCouple())
      {
        continue;
      }

      Vector3 treePos = tree.transform.position;

      // 1個目
      if (!isFound)
      {
        result = treePos;
        minDist = (charaPosition - treePos).magnitude;
        isFound = true;
        continue;
      }

      float dist = (charaPosition - treePos).magnitude;
      if (minDist > dist)
      {
        result = treePos;
        minDist = dist;
      }
    }
    return result;
  }
}
