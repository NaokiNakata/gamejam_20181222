﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapManager : SingletonMonoBehaviour<TrapManager>
{
  [SerializeField]
  GameObject TrapPrefab;

  [SerializeField]
  int TrapCost = 100;

  private List<GameObject> _trapArray = new List<GameObject>();

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {
    // 右クリックでトラップを置く
    if (Input.GetMouseButton(1))
    {
      Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
      RaycastHit hit = new RaycastHit();

      //レイを投げて何かのオブジェクトに当たった場合
      if (Physics.Raycast(ray, out hit))
      {
        if (hit.collider.tag == "Ground")
        {
          // レイが当たった位置(hit.point)にオブジェクトを生成する
          ClickGenerate(TrapPrefab, hit.point);
        }
      }
    }
  }

  // トラップのリストに追加
  public void AddTrap(GameObject trap)
  {
    _trapArray.Add(trap);
  }

  // クリックした位置にトラップを生成する
  void ClickGenerate(GameObject prefab, Vector3 clickPosition)
  {
    GameObject trap = GameObject.Instantiate(prefab, clickPosition, Quaternion.identity);
    AddTrap(trap);

    // トラップの生成コスト分スコアを減点
    ScoreManager.Instance.AddScore(-TrapCost);
  }

  // 与えられたキャラクターの座標に最も近いトラップの座標を返す
  public Vector3 GetNearestTrapPosition(Vector3 charaPosition)
  {
    Vector3 result = new Vector3(-1, -1, -1);
    if (!hasTrap())
    {
      return result;
    }

    float minDist = 0;

    bool isFound = false;
    foreach (GameObject trap in _trapArray)
    {
      // 無効状態なら削除
      if (!trap.gameObject.activeSelf)
      {
        _trapArray.Remove(trap);
        Destroy(trap.gameObject);
      }

      Vector3 trapPos = trap.transform.position;

      // 1個目
      if (!isFound)
      {
        result = trapPos;
        minDist = (charaPosition - trapPos).magnitude;
        isFound = true;
        continue;
      }

      float dist = (charaPosition - trapPos).magnitude;
      if (minDist > dist)
      {
        result = trapPos;
        minDist = dist;
      }
    }
    return result;
  }

  public bool hasTrap()
  {
    if (_trapArray.Count > 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
