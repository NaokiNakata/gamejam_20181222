﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : SingletonMonoBehaviour<UIController>
{
  // スコアのテキスト
  [SerializeField]
  public Text Score;

  // ゲーム終了のテキスト
  [SerializeField]
  public Text GameEnd;

  // Use this for initialization
  void Start()
  {
    GameEnd.gameObject.SetActive(false);
  }

  // Update is called once per frame
  void Update()
  {

  }

  public void UpdateScore(int newScore)
  {
    Score.text = newScore + " 点";
  }

  public void DisplayGameEnd()
  {
    GameEnd.text = "Fninish!! \n" + Score.text;
    GameEnd.gameObject.SetActive(true);
  }
}
