﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : SingletonMonoBehaviour<ScoreManager>
{

  private int _score = 0;

  // Use this for initialization
  void Start()
  {

  }

  // Update is called once per frame
  void Update()
  {

  }

  public void AddScore(int score)
  {
    _score += score;
    UIController.Instance.UpdateScore(_score);
  }
}
