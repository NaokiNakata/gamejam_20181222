﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
  [SerializeField]
  float gameTime = 60f;

  // ゲーム開始からの経過秒数
  private float _timer;

  private bool _isGameFinished = false;

  // Use this for initialization
  void Start()
  {
    _timer = gameTime;
    CharacterManager.Instance.StartGame(gameTime);
  }

  // Update is called once per frame
  void Update()
  {
    // Target生成状態の処理
    // timerを更新
    if (_timer > 0f)
    {
      _timer -= Time.deltaTime;
    }
    else
    {
      if (!_isGameFinished)
      {
        CharacterManager.Instance.StopGame();
        UIController.Instance.DisplayGameEnd();

        _isGameFinished = true;
      }
    }
  }
}
